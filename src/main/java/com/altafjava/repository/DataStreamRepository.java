package com.altafjava.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.altafjava.model.DataStream;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedScanList;

@Repository
public class DataStreamRepository {

	@Autowired
	private DynamoDBMapper dynamoDBMapper;

	public void insert(DataStream dataStream) {
		dynamoDBMapper.save(dataStream);
	}

	public PaginatedScanList<DataStream> findAll() {
		PaginatedScanList<DataStream> paginatedScanList = dynamoDBMapper.scan(DataStream.class, new DynamoDBScanExpression());
		return paginatedScanList;
	}

}
