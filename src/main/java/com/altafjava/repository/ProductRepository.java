package com.altafjava.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.altafjava.model.Product;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedScanList;

@Repository
public class ProductRepository {

	@Autowired
	private DynamoDBMapper dynamoDBMapper;

	public void insert(Product product) {
		dynamoDBMapper.save(product);
	}

	public PaginatedScanList<Product> findAll() {
		PaginatedScanList<Product> paginatedScanList = dynamoDBMapper.scan(Product.class, new DynamoDBScanExpression());
		return paginatedScanList;
	}

}
