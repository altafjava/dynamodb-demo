package com.altafjava.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.altafjava.model.DataStream;
import com.altafjava.repository.DataStreamRepository;

@RestController
public class DataStreamController {

	@Autowired
	private DataStreamRepository dataStreamRepository;

	@GetMapping("/save-data-stream")
	public String saveDataStreamIntoDynamoDB() {
		HSSFWorkbook hssfWorkbook = null;
		try {
			hssfWorkbook = new HSSFWorkbook(new FileInputStream("src/main/resources/DataStreams - altaf.xls"));
			HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);
			Iterator<Row> rowIterator = hssfSheet.rowIterator();
			int count = 0;
			while (rowIterator.hasNext()) {
				count++;
				if (count >= 10)
					return "only 10 allowed";
				Row row = rowIterator.next();
				if (count > 1) {
					String awsAccessKey = row.getCell(0).toString();
					long dataStreamId = (long) Float.parseFloat(row.getCell(1).toString());
					String dataStreamName = row.getCell(2).toString();
					int brandId = (int) Float.parseFloat(row.getCell(3).toString());
					String brandName = row.getCell(4).toString();
					int accountId = (int) Float.parseFloat(row.getCell(5).toString());
					String accountName = row.getCell(6).toString();
					String env = row.getCell(7).toString();
					DataStream dataStream = new DataStream();
					dataStream.setAccountId(accountId);
					dataStream.setAccountName(accountName);
					dataStream.setAwsAccessKey(awsAccessKey);
					dataStream.setBrandId(brandId);
					dataStream.setBrandName(brandName);
					dataStream.setDataStreamId(dataStreamId);
					dataStream.setDataStreamName(dataStreamName);
					dataStream.setEnv(env);
					dataStreamRepository.insert(dataStream);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "Datastream failed to save into dynamoDB";
		} finally {
			if (hssfWorkbook != null) {
				try {
					hssfWorkbook.close();
				} catch (IOException e) {
					return "hssfWorkbook closing failed";
				}
			}
		}
		return "Datastream saved into dynamoDB";
	}

	public static void main(String[] args) throws FileNotFoundException, IOException {
		HSSFWorkbook hssfWorkbook = new HSSFWorkbook(new FileInputStream("src/main/resources/DataStreams - altaf.xls"));
		HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(0);
		Iterator<Row> rowIterator = hssfSheet.rowIterator();
		int count = 0;
		while (rowIterator.hasNext()) {
			count++;
			Row row = rowIterator.next();
			if (count > 1) {
				String awsAccessKey = row.getCell(0).toString();
				long dataStreamId = (long) Float.parseFloat(row.getCell(1).toString());
				String dataStreamName = row.getCell(2).toString();
				int brandId = (int) Float.parseFloat(row.getCell(3).toString());
				String brandName = row.getCell(4).toString();
				int accountId = (int) Float.parseFloat(row.getCell(5).toString());
				String accountName = row.getCell(6).toString();
				String env = row.getCell(7).toString();
				DataStream dataStream = new DataStream();
				dataStream.setAccountId(accountId);
				dataStream.setAccountName(accountName);
				dataStream.setAwsAccessKey(awsAccessKey);
				dataStream.setBrandId(brandId);
				dataStream.setBrandName(brandName);
				dataStream.setDataStreamId(dataStreamId);
				dataStream.setDataStreamName(dataStreamName);
				dataStream.setEnv(env);
				System.out.println(dataStream);
//				System.out.println(
//						awsAccessKey + "   " + dataStreamId + "   " + dataStreamName + "   " + brandId + "   " + brandName + "   " + accountId + "  " + accountName + "   " + env);
			}
		}
		System.out.println("count==" + count);
	}
}
