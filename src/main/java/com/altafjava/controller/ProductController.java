package com.altafjava.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.altafjava.model.Product;
import com.altafjava.repository.ProductRepository;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedScanList;

@RestController
@RequestMapping("/products")
public class ProductController {

	@Autowired
	private ProductRepository productRepository;

	@PostMapping
	public String insertProduct(@RequestBody Product product) {
		productRepository.insert(product);
		return "Product inserted";
	}

	@GetMapping
	public PaginatedScanList<Product> findAllProducts() {
		return productRepository.findAll();
	}
}
